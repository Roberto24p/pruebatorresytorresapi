<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $users = User::all();
        $usersDto = collect($users)->map(function(User $u)  {
            $u['departament_name'] = $u->departament->name;
            $u['position_name'] = $u->position->name;
            return $u;
        });

        return response()->json([
            'success' =>1,
            'data' => $usersDto,
            'message' => ''
        ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request): JsonResponse
    {



        $user = User::create($request->all());
        return response()->json([
            'success' =>1,
            'data'=> $user,
            'message' => 'Usuario creado correctamente'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user):JsonResponse
    {
        return response()->json([
            'success' => 1,
            'data' => $user->load('departament')->load('position'),
            'message' => 'Usuario recuperado con éxito'
        ]);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUserRequest $request, User $user):JsonResponse
    {

        $user->update($request->all());

        return response()->json([
            'success' => 1,
            'data' => $user->load('departament')->load('position'),
            'message' => 'Usuario recuperado con éxito'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([
            'success' => 1,
            'data' => $user,
            'message' => "Usuario eliminado correctamente"
        ]);
    }
}
