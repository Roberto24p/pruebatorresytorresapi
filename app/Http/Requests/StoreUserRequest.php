<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'user'=> 'required|max:255',
            'firstName' => 'required|max:255',
            'secondName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'secondLastName' => 'required|max:255',
            'departament_id' => 'required|integer|not_in:0',
            'position_id'=>'required|max:255|not_in:0'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => 0,
            'message' => 'Ha ocurrido un error de validación.',
            'errors' => $validator->errors(),
        ], 200));
    }


}
