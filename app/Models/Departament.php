<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departament extends Model
{

    public $fillable = [
        'code',
        'name',
        'is_active',
        'user_crate_id'
    ];

    use HasFactory;
}
