<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Position extends Model
{
    use HasFactory;

    public $fillable = [
        'code',
        'name',
        'is_active',
        'user_crate_id'
    ];

    public function userCreate():BelongsTo{
        return $this->belongsTo(User::class, 'user_create_id');
    }
}
